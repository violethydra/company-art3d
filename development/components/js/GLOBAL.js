// ============================
//    Name: index.js
// ============================

import AddOpenBonusText from './modules/openBonusText';
import AddBackgroundSlider from './modules/backgroundSlider';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddOpenBonusText('.js__openBonusText', '.js__openBonusTextClose').run();
	new AddBackgroundSlider('js__sliderPicture', {
		pagination: '.js__sliderPagination',
		background: '.js__sliderBackground',
		sliderText: '.js__sliderText',
		items: '.gallary__nav-item',
		timerStop: false,
		timerSpeed: 3,
		timerResurrect: 6
	}).run();
};
if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
